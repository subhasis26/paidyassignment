package test;

/**
 * OrdinalIndicatorSuffixTest should have a function that take a number and return it as a string with the correct ordinal
 * indicator suffix (in English).
 * - numberToOrdinal(1) ==> '1st'
 * - numberToOrdinal(2) ==> '2nd'
 * - numberToOrdinal(3) ==> '3rd'
 *  -numberToOrdinal(4) ==> '4th'
 */
public class OrdinalIndicatorSuffixTest {
    private static final int EDGE_CASES = 10;

    String numberToOrdinal(int number) {
        if (number == 0) {
            return Integer.toString(0);
        }
        String ordinalSuffix = isEdgeCase(number) ? "th" : determineOrdinalSuffix(number);
        return Integer.toString(number).concat(ordinalSuffix);
    }

    private boolean isEdgeCase(int number) {
        int modeToTen = number % 10;
        int modeToHundred = number % 100;
        return ((modeToHundred - modeToTen) == EDGE_CASES);
    }

    private String determineOrdinalSuffix(Integer number) {
        int modeToTen = number % 10;
        switch (modeToTen) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }
    /*   Example ==>
    public static void main(String[] args) {
        OrdinalIndicatorSuffixTest ordinalIndicatorSuffixTest = new OrdinalIndicatorSuffixTest();
        System.out.println( ordinalIndicatorSuffixTest.numberToOrdinal(101));
    }
 */
}
