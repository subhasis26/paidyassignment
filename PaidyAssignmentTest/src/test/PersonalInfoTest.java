package test;

/*
returnMaskEmailPhoneNumber -  is a function which accepts the string input and validates it and later mask it
 */

public class PersonalInfoTest {

    String returnMaskEmailPhoneNumber(String userInput){
        String newString = "";
        if(userInput.contains("@")){
            newString = userInput.replaceAll("(?<=.{1}).(?=[^@]*?.@)", "*");
        }else if(userInput.startsWith("+")){
            newString = userInput.replaceAll("\\d(?=(?:\\D*\\d){4})", "*");
        }else{
            System.out.println("The string is not a valid one");
        }
        return newString;
    }

     /*   Example ==>
    public static void main(String[] args) {
        PersonalInfoTest personalInfoTest = new PersonalInfoTest();
        System.out.println( personalInfoTest.returnMaskEmailPhoneNumber("jotTheCot.3@gmail.com"));
        System.out.println( personalInfoTest.returnMaskEmailPhoneNumber("+91-902-233-22-3456"));
    }
      */
}