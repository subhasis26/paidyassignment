package test;

/*
 * countOfSundays -  is a function which accepts two inputs as string and return the count of sundays with in that time period
 * */

import java.text.SimpleDateFormat;
import java.util.Date;

public class ReturnNoOfSundaysTest {

    void countOfSundays(String startDate, String endDate) {
        int sundays = 0;
        try {
            Date start=new SimpleDateFormat("dd-mm-yyyy").parse(startDate);
            Date end=new SimpleDateFormat("dd-mm-yyyy").parse(endDate);
            int current = start.getDay();// 0 refers Sunday and 6 does Saturday
            if (current != 0)
                if (current != 0)
                    start.setDate(start.getDate() + (7 - current));//set the start date to very first coming Sunday

            while (start.before(end)) {
                sundays++;
                start.setDate(start.getDate() + 7);
            }
            if (end.getDay() == 0)
                sundays++;

            System.out.println("Total Sundays: " + sundays);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /*   Example ==>
    public static void main(String[] args) {
        ReturnNoOfSundaysTest returnNoOfSundaysTest = new ReturnNoOfSundaysTest();
        returnNoOfSundaysTest.countOfSundays("01-05-2022","30-05-2022");
    }
*/
}
